import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Question {

    ArrayList<Answer> answers = new ArrayList<>();
    String question;
    int numberOfPointsForQuestion;

    public Question(){

    }

    public Question(String question,int numberOfPointsForQuestion) {
        this.question = question;
        this.numberOfPointsForQuestion = numberOfPointsForQuestion;
    }

    public int getNumberOfPointsForQuestion() {
        return numberOfPointsForQuestion;
    }

    public String getQuestion() {
        return question;
    }

    public ArrayList<Answer> answersAdd(int numbersAnswer) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите ответы на вопрос");
        System.out.println("Если ответ на этот вопрос правильный введите - true, иначе false");

        for (int i = 0; i < numbersAnswer; i++) {
            String answer = reader.readLine();
            String rAnsw = reader.readLine();
            boolean rightAnswer = Boolean.parseBoolean(rAnsw);
            answers.add(new Answer(answer,rightAnswer));
        }

        return answers;
    }


    public void outputOfAnswers() {
        for (int i = 0; i < answers.size(); i++) {
            System.out.println((i+1)+")"+answers.get(i).getAnswer());
        }
    }

}
