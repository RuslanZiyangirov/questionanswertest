public class RightAnswer {

    int rightAnswer;
    boolean flag;

    public RightAnswer(int rightAnswer){
        this.rightAnswer = rightAnswer;
        this.flag = true;
    }

    public int getRightAnswer() {
        return rightAnswer;
    }

    public boolean isFlag() {
        return flag;
    }
}
