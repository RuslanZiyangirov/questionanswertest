public class Answer {


    boolean rightAnswerBool;
    String answer;

    public Answer(String answer,boolean rightAnswerBool ){
        this.answer = answer;
        this.rightAnswerBool = rightAnswerBool;


    }

    public boolean isRightAnswerBool() {
        return rightAnswerBool;
    }

    public String getAnswer() {
        return answer;
    }
}
