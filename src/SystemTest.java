import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

public class SystemTest {


    public void sysTest() throws IOException {
        ArrayList<Question> questions = new ArrayList<>();
        Question questionObject = new Question();
        System.out.println("Введите какое будет кол-во вопросов будет в 'БАЗА ВОПРОСОВ':");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String sNumber = reader.readLine();
        int numbersQuestion = Integer.parseInt(sNumber);


        for (int i = 0; i < numbersQuestion; i++) {
            System.out.println("Введите вопрос:");
            System.out.println("Введите кол-во баллов за вопрос:");
            String question = reader.readLine();
            String nOfPForQu = reader.readLine();
            int numberOfPointsForQuestion = Integer.parseInt(nOfPForQu);
            questions.add(new Question(question, numberOfPointsForQuestion));

            System.out.println("Введите число ответов на этот вопрос:");
            String sNum = reader.readLine();
            int numbersAnswer = Integer.parseInt(sNum);
            questions.get(i).answersAdd(numbersAnswer);
        }

        System.out.println("Укажите кол-во заданий в тесте");
        ArrayList<Question> assignmentList = new ArrayList<>();

        while (true) {
            String assign = reader.readLine();
            int assignment = Integer.parseInt(assign);
            if (assignment > questions.size()) {
                System.out.println("Вы ввели заданий больше, чем есть вопросов. Попробуйте заново!");
            } else {

                for (int i = 0; i < assignment; i++) {
                    assignmentList.add(questions.get(i));
                    Collections.shuffle(assignmentList);
                }
                break;
            }
        }

        System.out.println("------------------ Тест начался! ------------------");

        Date currentTime = new Date();

        int rightNumberAnswer = 0;
        int count = 0;
        int transientVariable = 0;
        ArrayList<Question> resultOfQuestions = new ArrayList<>();
        ArrayList<Answer> listOfCorrectAnswersToQuestions = new ArrayList<>();

        for (int i = 0; i < assignmentList.size(); i++) {
            System.out.println(assignmentList.get(i).getQuestion());
            assignmentList.get(i).outputOfAnswers();
            System.out.println("Ответ (введите номер ответа) : ");
            String rAnsw = reader.readLine();
            int rAnswInt = Integer.parseInt(rAnsw);


            RightAnswer rightAnswer = new RightAnswer(rAnswInt);

            for (int j = 0; j < assignmentList.get(i).answers.size(); j++) {
                if (rAnswInt == (j + 1) &&
                        rightAnswer.isFlag() == assignmentList.get(i).answers.get(j).isRightAnswerBool()) {

                    rightNumberAnswer = rightNumberAnswer + 1;
                    count += assignmentList.get(i).getNumberOfPointsForQuestion();

                }if (rAnswInt == (j + 1) &&
                        rightAnswer.isFlag() != assignmentList.get(i).answers.get(j).isRightAnswerBool()){
                    resultOfQuestions.add(assignmentList.get(i));
                }
            }


        }

        for (int i = 0; i < resultOfQuestions.size(); i++) {
            for (int j = 0; j < resultOfQuestions.get(i).answers.size(); j++) {
                if(resultOfQuestions.get(i).answers.get(j).isRightAnswerBool()) {
                    listOfCorrectAnswersToQuestions.add(resultOfQuestions.get(i).answers.get(j));
                }
            }

        }
        Date newTime = new Date();


        System.out.println("------------------ Тест закончился! ------------------");
        System.out.println("Список вопросов, на которые пользователь ответил неверно:");
        System.out.println();
        for (Question resultOfQuestion : resultOfQuestions) {
            System.out.println(resultOfQuestion.getQuestion());
        }
        System.out.println();

        System.out.println("Cписок правильных ответов на эти вопросы:");
        System.out.println();

        for (Answer listOfCorrectAnswersToQuestion : listOfCorrectAnswersToQuestions) {
            System.out.println(listOfCorrectAnswersToQuestion.getAnswer());
        }
        System.out.println();
        System.out.println("Количество правильных ответов - " + rightNumberAnswer);
        System.out.println("Количество набранных баллов - " + count);
        System.out.println("Ушло время на выполнение теста:" + (newTime.getTime() - currentTime.getTime())/1000 +" секунд.");
    }


}

